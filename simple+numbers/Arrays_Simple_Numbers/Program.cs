﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrays_Simple_Numbers
{
    class Program
    {
        private static int inputNumber(string message)
        {
            int getNumber;
            string enteredNumber = "";
            while (!int.TryParse(enteredNumber, out getNumber))
            {
                Console.WriteLine(message);
                enteredNumber = Console.ReadLine();
            }
            return getNumber;
        }
        
        private static void fillArrayWithNumbers(int row, int n, int number, int column, int[,] AllNumbers)
        {
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < column; j++)
                {
                    if (n <= number)
                    {
                        AllNumbers[i, j] = n;
                    }
                    n++;
                }
            }
        }

        private static int DetermineRows(int number, int column)
        {
            int row;
            if ((number % column != 0))
            {
               return row = (number / column) + 1;
            }
            else
            {
               return row = number / column;
            }
        }

        private static void FindPrimeNumbers(int[,] AllNumbers, int i, int j)
        {
            foreach (int p in AllNumbers)
            {
                if (AllNumbers[i, j] > p)
                {
                    if ((AllNumbers[i, j] == 2 && p == 2) || (AllNumbers[i, j] == 0))
                    {
                        break;
                    }
                    else if (p == 0)
                    {
                        continue;
                    }
                    else if (AllNumbers[i, j] % p == 0 && AllNumbers[i, j] > p)
                    {
                        AllNumbers[i, j] = 0;
                        break;
                    }
                    continue;
                }
                break;
            }
        }

        private static void PrintToScreen(int[,] AllNumbers, int row, int column)
        {
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < column; j++)
                {
                    Console.Write("{0,5}", AllNumbers[i, j]);
                }
                Console.WriteLine();
            }
        }
        
        static void Main(string[] args)
        {
            //int row, n = 2, number = 0, column = 0;
            //number = inputNumber("Enter whole number");
            //column = inputNumber("Enter columns number");
            //row = DetermineRows(number, column);

            //int[,] AllNumbers = new int[row, column];
            //fillArrayWithNumbers(row, n, number, column, AllNumbers);

            //for (int i = 0; i < row; i++)
            //{
            //    for (int j = 0; j < column; j++)
            //    {
            //        FindPrimeNumbers(AllNumbers, i, j);
            //    }
            //}

            //PrintToScreen(AllNumbers, row, column);

            int row, n = 2, number = 0, column = 0;
            number = inputNumber("Enter whole number");
            column = inputNumber("Enter columns number");
            row = DetermineRows(number, column);

            int[,] AllNumbers = new int[row, column];
            fillArrayWithNumbers(row, n, number, column, AllNumbers);



            Console.ReadLine();
            Console.ReadLine();
        }
    }
}
