﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BubbleSort
{
    class Program
    {
        private static void SwapNumbers(int[] MyArray, int i)
        {
            int tempNumber = MyArray[i];
            MyArray[i] = MyArray[i + 1];
            MyArray[i + 1] = tempNumber;
        }

        private static void BubbleSort(int[] MyArray)
        {
            for (int j = 0; j < (MyArray.Length - 1); j++)
            {
                for (int i = 0; i < (MyArray.Length - 1); i++)
                {
                    if (MyArray[i] > MyArray[i + 1])
                    {
                        SwapNumbers(MyArray, i);
                    }
                }
            }
        }
        private static void Print(int[] MyArray)
        {
            foreach (int number in MyArray)
            {
                Console.Write("{0,5}", number);
            }
        }

        private static void FillArrayWithRandomNumbers(int[] MyArray)
        {

            //int[] arr = new Random(100) int[];

            Random random = new Random(100);
            
            for (int i = 0; i < MyArray.Length; i++)
            {
                
                MyArray[i] = random.Next(1,100);

            }

        }

        static void Main(string[] args)
        {
            int n = 100;
            int[] RandomNumbersArray = new int[n];
            FillArrayWithRandomNumbers(RandomNumbersArray);
            BubbleSort(RandomNumbersArray);
            Print(RandomNumbersArray);

            Console.ReadLine();
        }
    }
}
