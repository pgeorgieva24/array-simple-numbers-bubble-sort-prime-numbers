﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace simple_numbers
{
     class IsPrimeNumber
    {
        public IsPrimeNumber(int num)
        {
            bool isPrime;
            int sumOfPrimes = 0;
            List<int> ListOfPrimes = new List<int> {};
            
            for (int i = 2; i <= num; i++)
            {
                isPrime = true;
                foreach (int p in ListOfPrimes)
                {
                    if (i % p == 0)
                    {
                        isPrime = false;
                        break;
                    }
                }
                if (isPrime)
                {
                    ListOfPrimes.Add(i);
                }
            }
            //ListOfPrimes.Insert(0, 1);

            Console.WriteLine();
            if (num == 1) Console.WriteLine("1 is not a prime number.");
            else Console.Write("All prime numbers between 0 and " + num + " are: ");
            foreach (int p in ListOfPrimes)
            {
                Console.Write(p + ", ");
            }
            Console.WriteLine();

            foreach (int p in ListOfPrimes)
            {
                sumOfPrimes += p;
            }
            Console.WriteLine("The sum of all prime numbers is: " + sumOfPrimes);
            Console.ReadLine();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Enter A Number");
            int number =Convert.ToInt32(Console.ReadLine());
            IsPrimeNumber PM = new IsPrimeNumber (number);

        }
    }
}
